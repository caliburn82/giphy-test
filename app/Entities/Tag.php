<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    // --------------------------------------------- events


    // --------------------------------------------- relations

    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    // --------------------------------------------- scopes
    // --------------------------------------------- helpers


}
