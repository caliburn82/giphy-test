<?php


namespace App\Searchers;


use App\Entities\Image;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorContract;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class ExternalSearch extends SearchAbstract
{

    public function find(...$tags): LengthAwarePaginatorContract
    {


        // run external search and retrieve data
        $url        = config('giphy.api.url');
        $query      = config('giphy.api.options');
        $query['q'] = implode(' ', $tags);

        $request     = $this->request;
        $perPage     = $request->per_page ?: static::PER_PAGE;
        $currentPage = $request->page ?: 1;

        $query['limit']  = $perPage;
        $query['offset'] = $perPage * ($currentPage - 1);

        $query = http_build_query($query);


        $url  = sprintf('%s/search?%s', $url, $query);
        $json = file_get_contents($url);
        $data = json_decode($json);

        // map and store supplied data
        // handle within a single transaction for faster inserts

        /** @var Collection $result */
        $result = \DB::transaction(function () use ($data) {

            return collect($data->data)->map(function ($row) {

                return Image::create([
                    'id'        => $row->id,
                    'type'      => $row->type,
                    'title'     => $row->title,
                    'slug'      => $row->slug,
                    'url'       => $row->url,
                    'embed_url' => $row->embed_url,
                    'still_url' => $row->images->original_still->url,
                    'width'     => $row->images->original->width,
                    'height'    => $row->images->original->height,
                    'frames'    => $row->images->original->frames,
                ]);
            });

        });

        return app()->make(LengthAwarePaginator::class, [
            'items'        => $result,
            'total'        => $data->pagination->total_count,
            'perPage'      => $perPage,
            'currentPage' => $currentPage,
            'options'      => [
                'path' => Paginator::resolveCurrentPath(),
            ],

        ]);

    }


}
