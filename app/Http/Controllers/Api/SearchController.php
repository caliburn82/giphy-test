<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Searchers\ExternalSearch;
use App\Searchers\LocalSearch;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    const PER_PAGE = 15;

    public function search(Request $request)
    {

        $tags = $request->tags ?: [];

        if (is_string($tags)) {
            $tags = preg_replace('/[^a-z0-9]+/i', ',', $tags);
            $tags = explode(',', $tags);
        }

        $searchType = LocalSearch::class;
        $request->type === 'external' && $searchType = ExternalSearch::class;


        return app()->make($searchType)->find(...$tags);

    }


}
