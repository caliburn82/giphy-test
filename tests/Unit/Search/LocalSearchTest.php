<?php

namespace Tests\Unit\Search;

use App\Entities\Image;
use App\Entities\Tag;
use App\Searchers\LocalSearch;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class LocalSearchTest extends TestCase
{

    use RefreshDatabase, WithFaker;


    protected function setUp(): void
    {
        parent::setUp();

        factory(Image::class)->times(50)->create();
    }


    public function testBlankSearch()
    {
        // blank searches should come back with a random result set

        $request = new Request();
        $results = (new LocalSearch($request))->find();

        $this->assertInstanceOf(LengthAwarePaginator::class, $results);
        $this->assertCount(LocalSearch::PER_PAGE, $results->items(), 'count does not match default pagination');

        $this->assertInstanceOf(Image::class, $results->items()[0]);

        // make sure pagination options still work
        $request->replace(['per_page' => 10]);
        $results = (new LocalSearch($request))->find();
        $this->assertCount(10, $results->items(), 'results per page isn\'t matching up');


        // we shouldn't get a second page of results
        $request->replace(['page' => 2]);
        $results = (new LocalSearch($request))->find();
        $this->assertCount(0, $results->items(), 'second page returns results when it shouldn\'t');
    }


    public function testTagSearch()
    {

        $tag1 = factory(Tag::class)->create();
        $tag2 = factory(Tag::class)->create();
        $tag3 = factory(Tag::class)->create();

        ($image1 = factory(Image::class)->create())->tags()->attach($tag1);
        ($image2 = factory(Image::class)->create())->tags()->attach([$tag1->id, $tag2->id]);
        ($image3 = factory(Image::class)->create())->tags()->attach($tag2->id);
        // unused control field
        ($image4 = factory(Image::class)->create())->tags()->attach($tag3->id);

        $request = new Request();

        // test with single tag first
        $results = (new LocalSearch($request))->find($tag1->name);
        $this->assertInstanceOf(LengthAwarePaginator::class, $results);
        $this->assertCount(2, $results->items());

        $this->assertContains($image1->id, $results->pluck('id'));
        $this->assertContains($image2->id, $results->pluck('id'));
        $this->assertNotContains($image3->id, $results->pluck('id'));
        $this->assertNotContains($image4->id, $results->pluck('id'));


        // test with multiple search
        $results = (new LocalSearch($request))->find($tag1->name, $tag2->name);
        $this->assertCount(3, $results->items());
        $this->assertNotContains($image4->id, $results->pluck('id'));

    }
}
