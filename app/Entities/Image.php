<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Image extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;


    protected $fillable = [
        'id', 'type', 'title', 'slug',
        'url', 'embed_url', 'still_url',
        'width', 'height', 'frames',
    ];


    // --------------------------------------------- events


    protected static function boot()
    {
        parent::boot();

        // strip tags from title and associate to new
        self::created(function (Image $image) {

            // we will want to have this within a single transaction where possible multiple inserts will otherwise be slow

            // are we already within a transaction?
            if (\DB::transactionLevel() > 0) {
                $image->updateTags();
                return;
            }

            \DB::transaction(function () use ($image) {
                $image->updateTags();
            });

        });

    }


    // --------------------------------------------- relations

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // --------------------------------------------- scopes


    public function scopeHasTags(Builder $query, ...$tags)
    {

        $query
            ->leftJoin('image_tag', "images.id", '=', "image_tag.image_id")
            ->leftJoin('tags', "tags.id", '=', "image_tag.tag_id")
            ->whereIn('tags.name', $tags);

    }


    // --------------------------------------------- helpers


    private function updateTags()
    {
        $title = preg_replace('/[^a-z0-9]+/i', ' ', $this->title);
        $tags  = explode(' ', strtolower(trim($title)));

        // call all at once rather than individual queries as it's quicker
        $knownTags = Tag::whereIn('name', $tags)->pluck('id', 'name');
        collect($tags)->unique()->reduce(function (Collection $known, $tag) {

            // skip short words
            if (strlen($tag) < 3) {
                return $known;
            }

            // todo: remove conjunctions as well

            if ($id = $known->get($tag)) {
                $this->tags()->attach($id);
                return $known;
            }

            $tag = $this->tags()->create(['name' => $tag]);
            return $known->put($tag->name, $tag->id);
        }, $knownTags);


        return $this;

    }
}
