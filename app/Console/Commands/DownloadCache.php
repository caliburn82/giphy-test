<?php

namespace App\Console\Commands;

use App\Entities\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Helper\ProgressBar;

class DownloadCache extends Command
{

    /** @var ProgressBar */
    protected $progressBar;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:giphy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads a set number of records using the GIPHY API and stores to local cache database';
    /**
     * @var \Illuminate\Config\Repository
     */

    /** @var int */
    private $max, $delay, $requests = 0;

    /** @var string */
    private $url;

    /** @var Collection */
    private $knownIds;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


        $this->max   = config('giphy.api.max_images');
        $this->delay = config('giphy.api.delay');

        $url       = config('giphy.api.url');
        $query     = http_build_query(config('giphy.api.options'));
        $this->url = sprintf('%s/random?%s', $url, $query);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $start          = microtime(true);
        $this->knownIds = Image::pluck('id'); // used to avoid duplicates later

        $count     = $this->knownIds->count();
        $remaining = $this->max - $count;

        if ($remaining <= 0) {
            $this->warn('Max number of records already exist. Please check config.');
            return;
        }

        $this->progressBar = $this->output->createProgressBar($remaining);

        $this->info("$count records found. $remaining records remaining.");
        $this->output->writeln('');


        // download and store data until we've reached the max
        while ($this->knownIds->count() < $this->max) {

            try {

                $this->updateCache();

            } catch (\Exception $ex) {
                report($ex);
                $this->error('<error>An error occurred while attempting update. Please view logs for more information</error>');
            }

            $this->delay > 0 && usleep($this->delay * 1000000);
        }

        $this->progressBar->finish();

        $this->output->writeln('');
        $this->info('Download complete');

        $time = microtime(true) - $start;
        $this->info('Total processing time: ' . round($time, 2) . 's');
        $this->info('Total requests: ' . $this->requests);

        return;
    }


    private function updateCache()
    {
        $json = file_get_contents($this->url);
        $data = json_decode($json)->data;
        $this->requests++;

        // skip if we've seen this one
        if ($this->knownIds->contains($data->id)) {
            return false;
        }

        Image::create([
            'id'        => $data->id,
            'type'      => $data->type,
            'title'     => $data->title,
            'slug'      => $data->slug,
            'url'       => $data->url,
            'embed_url' => $data->embed_url,
            'still_url' => $data->images->original_still->url,
            'width'     => $data->image_width,
            'height'    => $data->image_height,
            'frames'    => $data->image_frames,
        ]);

        $this->knownIds->push($data->id);
        $this->progressBar->advance();

        return true;
    }


}
