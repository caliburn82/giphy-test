# Giphy Test
This app was created to demonstrate the use of API end-points as well as some front-end technologies. This is purely for
demonstration purposes and is not meant for use in a commercial environment.

This application was developed using PHP 7.3 with MySql 5.7.

## Installation
Requirements:
* [Composer](https://getcomposer.org/)
* A [Giphy](https://developers.giphy.com/) API Key
* [Node.js](https://nodejs.org)
    * Optional - this is only required if you would like to make modifications to the front-end code

Once the repository has been cloned, you will need to run composer install to download all required packages. Make sure
to setup the .env file (or use environment variables) to connect to your database and run all laravel migrations. This
can be done by running `php artisan migrate` from the console within the base directory of the application.

Add your Giphy API key to the .env file (or using environment variables) under the variable name: "GIPHY_API_KEY"

### Initial setup
Once everything is ready to go, you will need to setup an initial cache from giphy. To do this, run
`php artisan download:giphy`. This will download a configurable number of items for the initial cache for this project.

### Web server
Make sure to point your web server to use the 'public' directory for this application.


## Additional
This section shows additional files and information which may be important.

### Database backup
A mysql query has been created which will create a copy of the 'images' table called 'images_2'. This will be identical
to the images table, however the 'title' field will have an additional timestamp suffix.

This file can be found here: database/images_2.sql

## API Settings

There is a single API end-point for search for both locally cached, or externally hosted images from Giphy. This can be
found at '/api/v1/search'.

### Parameters
* type
    * external / local expected.
    * defaults to 'local'
* tags
    * this can be a single tag, a csv, an array, or a phrase (space separated)
    * omitted on a local search type will return a single page of random images
* page
    * results page number. defaults to 1
* per_page
    * how many results to return on a single page
    
Additionally, any images retrieved externally will be stored locally for future searches

### Testing
This section was built with unit testing and so any changes to the API will need updates to how unit and feature tests
are handled.



