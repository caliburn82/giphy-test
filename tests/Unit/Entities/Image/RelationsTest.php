<?php

namespace Tests\Unit\Entities\Image;

use App\Entities\Image;
use App\Entities\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RelationsTest extends TestCase
{

    use RefreshDatabase;


    public function testTags()
    {
        // make sure title is blank to avoid auto-creating tags for this test
        $image = factory(Image::class)->create(['title' => '']);

        $tag1 = factory(Tag::class)->create();
        $tag2 = factory(Tag::class)->create();

        // add an extra for control
        $tag3 = factory(Tag::class)->create();

        \DB::table('image_tag')->insert([
            ['image_id' => $image->id, 'tag_id' => $tag1->id],
            ['image_id' => $image->id, 'tag_id' => $tag2->id],
        ]);

        $tags = $image->tags;
        $this->assertInstanceOf(Collection::class, $tags);
        $this->assertCount(2, $tags);

        // make sure we're receiving the correct items
        $first = $tags->first();
        $this->assertInstanceOf(Tag::class, $first);
        $this->assertEquals($tag1->id, $first->id);


        // make sure we never see the extra image
        $this->assertEquals(0, $image->tags()->where('id', $tag3->id)->count());

    }
}
