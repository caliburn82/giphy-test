<?php


namespace App\Searchers;


use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorContract;

interface Hunter
{

    public function find(...$tags): LengthAwarePaginatorContract;


}
