<?php

return [

    'api' => [
        'max_images' => env('GIPHY_MAX_IMAGES', 500),
        'delay'      => env('GIPHY_API_DELAY', 0), // time in seconds between each call
        'url'        => 'https://api.giphy.com/v1/gifs',
        'options'    => [
            'api_key' => env('GIPHY_API_KEY'),
            'rating'  => 'G',
        ],
    ],


];
