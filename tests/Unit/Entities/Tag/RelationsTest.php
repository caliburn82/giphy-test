<?php

namespace Tests\Unit\Entities\Tag;

use App\Entities\Image;
use App\Entities\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RelationsTest extends TestCase
{

    use RefreshDatabase;


    public function testImages()
    {

        /** @var Tag $tag */
        $tag = factory(Tag::class)->create();

        /** @var Tag $tag */
        $image1 = factory(Image::class)->create();
        $image2 = factory(Image::class)->create();

        // add an extra for control
        $image3 = factory(Image::class)->create();

        \DB::table('image_tag')->insert([
            ['tag_id' => $tag->id, 'image_id' => $image1->id],
            ['tag_id' => $tag->id, 'image_id' => $image2->id],
        ]);

        $images = $tag->images;
        $this->assertInstanceOf(Collection::class, $images);
        $this->assertCount(2, $images);

        // make sure we're receiving the correct items
        $first = $images->first();
        $this->assertInstanceOf(Image::class, $first);
        $this->assertEquals($image1->id, $first->id);


        // make sure we never see the extra image
        $this->assertEquals(0, $tag->images()->where('id', $image3->id)->count());

    }
}
