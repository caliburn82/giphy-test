<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'id'        => \Str::random(20), // non-unique but low chance of duplicate
        'type'      => 'gif',
        'title'     => $faker->text(50),
        'slug'      => $faker->slug,
        'url'       => $faker->imageUrl(),
        'embed_url' => $faker->imageUrl(),
        'still_url' => $faker->imageUrl(),
        'width'     => $faker->numberBetween(10, 600),
        'height'    => $faker->numberBetween(10, 600),
        'frames'    => $faker->numberBetween(20, 200),
    ];
});
