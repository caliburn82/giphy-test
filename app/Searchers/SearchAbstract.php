<?php


namespace App\Searchers;


use Illuminate\Http\Request;

abstract class SearchAbstract implements Hunter
{

    const PER_PAGE = 15;

    /** @var Request */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


}
