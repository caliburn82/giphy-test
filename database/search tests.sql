# % filtered by index: 100, 100, 100
# rows: 500, 3, 1
explain
    select distinct id, title
    from images
    where exists(select *
                 from tags
                          inner join image_tag on tags.id = image_tag.tag_id
                 where images.id = image_tag.image_id
                   and name in ('wedding', 'photo', 'booth')
              );



# % filtered by index: 100, 10, 100 (100, 100, 100 with individual indexes added to keys - fixed it!)
# rows: 3, 2, 1
explain # use this one with foreign keys or individual indexes enabled
    select images.id, images.title, count(image_tag.tag_id) as relevance
    from images
             left join image_tag on images.id = image_tag.image_id
             left join tags on image_tag.tag_id = tags.id
    where tags.name in ('wedding', 'photo', 'booth', 'machine', 'cutting')
    group by images.id
    order by relevance desc;



# % filtered by index: 100, 100, 100
# rows: 3, 2, 1
explain
    select images.id, images.title, count(image_tag.tag_id) as relevance
    from tags
             left join image_tag on tags.id = image_tag.tag_id
             left join images on image_tag.image_id = images.id
    where tags.name in ('wedding', 'photo', 'booth')
    group by images.id
    order by relevance desc;
