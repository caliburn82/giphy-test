<?php

namespace Tests\Unit\Search;

use App\Entities\Image;
use App\Searchers\ExternalSearch;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class ExternalSearchTest extends TestCase
{

    use RefreshDatabase, WithFaker;


    public function testTagSearch()
    {


        // test with single tag first
        $request = new Request();
        $results = (new ExternalSearch($request))->find('life robots, and everything');

        // check new images are stored to the database cache
        $this->assertInstanceOf(LengthAwarePaginator::class, $results);
        $this->assertEquals(15, Image::count());
        $this->assertGreaterThan(15, $results->total());
        $firstImage = $results->items()[0];
        $this->assertInstanceOf(Image::class, $firstImage);


        // test pagination
        $request->replace(['page' => 2]);
        $results = (new ExternalSearch($request))->find('life robots, and everything');
        // new set of fields
        $this->assertEquals(30, Image::count());
        $this->assertEquals(2, $results->currentPage());

    }


}
