<?php

namespace Tests\Unit\Entities\Image\Events;

use App\Entities\Image;
use App\Entities\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreatedTest extends TestCase
{

    use RefreshDatabase;


    public function testTagMatching()
    {


        $knownTag1 = factory(Tag::class)->create(['name' => 'test']);
        $knownTag2 = factory(Tag::class)->create(['name' => 'title']);

        // make sure we have the same word more than once for duplicate check
        $image = factory(Image::class)->create(['title' => 'this test is a predictable test title']);

        // existing tags associated
        $this->assertDatabaseHas('image_tag', ['image_id' => $image->id, 'tag_id' => $knownTag1->id]);
        $this->assertDatabaseHas('image_tag', ['image_id' => $image->id, 'tag_id' => $knownTag2->id]);


        // test new tag added and associated
        $id = Tag::where('name', 'predictable')->value('id');
        $this->assertNotNull($id); // should be added automagically
        $this->assertDatabaseHas('image_tag', ['image_id' => $image->id, 'tag_id' => $id]);

    }
}
