<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();


            $table->string('type');
            $table->string('title');
            $table->string('slug');
            $table->string('url');
            $table->string('embed_url');
            $table->string('still_url');
            $table->unsignedInteger('width');
            $table->unsignedInteger('height');
            $table->unsignedInteger('frames');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
