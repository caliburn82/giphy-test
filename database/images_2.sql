create table images_extended
select id,
       created_at,
       updated_at,
       type,
       concat(title, ' - ', CURRENT_TIMESTAMP) as title,
       slug,
       url,
       embed_url,
       still_url,
       width,
       height,
       frames
from images;
