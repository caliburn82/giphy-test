<?php

namespace Tests\Unit\Entities\Image\Scopes;

use App\Entities\Image;
use App\Entities\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HasTagsTest extends TestCase
{

    use RefreshDatabase;

    public function testSingleSearch()
    {

        factory(Image::class)->times(50)->create();

        $tag    = Tag::inRandomOrder()->first();
        $images = $tag->images;

        $result = Image::hasTags($tag->name)->get('images.*');

        // make sure we get the same number of results back and that the ID's match up
        $this->assertCount($images->count(), $result);
        $this->assertEquals($images->pluck('id'), $result->pluck('id'));

    }


}
