<?php


namespace App\Searchers;


use App\Entities\Image;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorContract;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class LocalSearch extends SearchAbstract
{


    public function find(...$tags): LengthAwarePaginatorContract
    {
        if (empty($tags)) {
            return $this->fetchRandom();
        }

        $request     = $this->request;
        $perPage     = $request->per_page ?: static::PER_PAGE;
        $currentPage = $request->page ?: 1;

        // relevance is measured by images which match the most tags.
        return Image::hasTags(...$tags)
            ->selectRaw('count(tags.id) as relevance')
            ->addSelect('images.*')
            ->groupBy('images.id')
            ->orderBy('relevance', 'desc')
            ->paginate($perPage, ['*'], 'page', $currentPage);
    }


    private function fetchRandom(): LengthAwarePaginator
    {
        $request     = $this->request;
        $perPage     = $request->per_page ?: static::PER_PAGE;
        $currentPage = $request->page ?: 1;

        $data = ($currentPage == 1)
            ? Image::inRandomOrder()->limit($perPage)->get()
            : new Collection();

        return app()->make(LengthAwarePaginator::class, [
            'items'   => $data,
            'total'   => $data->count(),
            'perPage' => $perPage,
            'options' => [
                'path' => Paginator::resolveCurrentPath(),
            ],

        ]);

    }
}
